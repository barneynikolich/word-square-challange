package com.company;

/**
 * @author BarneyN
 * @since 27/03/2019
 */
public class Column
{
    char character;

    Column(char character)
    {
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }
}
