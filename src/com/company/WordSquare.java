package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author BarneyN
 * @since 27/03/2019
 */
public class WordSquare {
    private int size;
    private ArrayList<Character> originalAvailavbleChars;
    private ArrayList<Character> availableCharacters;
    private ArrayList<String> wordList;
    private Set charSet = new HashSet();
    private WordService wordService = new WordService();

    ArrayList<Row> rows = new ArrayList<>();

    WordSquare(int size, String letters, ArrayList wordList) {
        this.size = size;
        this.availableCharacters = adaptToArrayList(letters);
        this.originalAvailavbleChars = adaptToArrayList(letters);
        this.wordList = wordList;
        for (char letter : this.availableCharacters) {
            this.charSet.add(letter);
        }

    }

    private void resetAndUpdateAvailableCharacters(Row... rows) {
        this.availableCharacters = originalAvailavbleChars;
        for (Row row : rows) {
            this.availableCharacters = updateAvailableLetters(row.getWord());
        }
    }

    private void updateAvailableCharacters(String word) {
        this.availableCharacters = updateAvailableLetters(word);
    }


    private List<String> getPossibleWords(List<String> availableWords, char startChar)
    {
        List<String> availableWordsz = wordService.getPossibleWordsFromCharacterSet(availableCharacters, availableWords, size);
        return getWordsStartingWithChar(availableWordsz, startChar);
    }

    public void solve() {
        List<String> availableWords = wordService.getPossibleWords(charSet, wordList, size);

        int attemptCounter = 0;

        boolean puzzleSolved = false;
        while (!puzzleSolved) {

            for (String word : availableWords) {
                attemptCounter++;
                if (puzzleSolved)
                {
                    break;
                }

                this.availableCharacters = originalAvailavbleChars; // reset characters as attempting next word

                Row initialRow = createInitialRow(word);

                List<String> specificWords = getPossibleWords(availableWords, initialRow.getColumns().get(1).getCharacter());

                for (int i = 0; i < specificWords.size(); i++) {
                    if (puzzleSolved)
                    {
                        break;
                    }
                    resetAndUpdateAvailableCharacters(initialRow);

                    Row row2 = new Row(specificWords.get(i));
                    updateAvailableCharacters(row2.getWord());

                    List<String> specificWord2 = getPossibleWords(availableWords, initialRow.getColumns().get(2).getCharacter());

                    if (specificWord2.size() == 0) {
                        continue;
                    } else {
                        for (int j = 0; j < specificWord2.size(); j++) {
                            if (puzzleSolved)
                            {
                                break;
                            }

                            resetAndUpdateAvailableCharacters(initialRow, row2);

                            Row row3 = createRow(specificWord2.get(j));

                            char thirdRowSecondChar = row3.getColumns().get(1).getCharacter();
                            char secondRowThirdChar = row2.getColumns().get(2).getCharacter();
                            if (thirdRowSecondChar == secondRowThirdChar) {

                                List<String> specificWord3 = getPossibleWords(availableWords, initialRow.getColumns().get(3).getCharacter());

                                if (specificWord3.size() == 0) {
                                    // No words left matching available character set; try next word
                                    continue;
                                } else {
                                    for (int k = 0; k < specificWord3.size(); k++) {
                                        Row row4 = new Row(specificWord3.get(k));
                                        updateAvailableCharacters(row4.getWord());

                                        char secondRow4thChar = row2.getColumns().get(3).getCharacter();
                                        char fourthRow2ndChar = row4.getColumns().get(1).getCharacter();
                                        char thirdRow4thChar = row3.getColumns().get(3).getCharacter();
                                        char fourthRow3rdChar = row4.getColumns().get(2).getCharacter();

                                        if ((secondRow4thChar == fourthRow2ndChar) && (thirdRow4thChar == fourthRow3rdChar)) {
                                            this.rows.addAll(Arrays.asList(initialRow, row2, row3, row4));
                                            puzzleSolved = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        System.out.println(attemptCounter);
        this.rows.forEach(row -> System.out.println("row: " + row.getWord()));
    }

    private Row createRow(String word)
    {
        Row row = new Row(word);
        updateAvailableCharacters(word);
        return row;
    }

    private Row createInitialRow(String word) {
        Row row = new Row(word);
        updateAvailableCharacters(row.getWord());
        return row;
    }

    private ArrayList<Character> updateAvailableLetters(String usedLetters) {
        ArrayList<Character> availableChars = (ArrayList) availableCharacters.clone();

        for (Character usedLetter : usedLetters.toCharArray()) {
            for (int i = 0; i < availableChars.size(); i++) {
                char available = availableChars.get(i);
                if (available == usedLetter) {
                    availableChars.remove(i);
                    break;
                }
            }
        }
        return availableChars;
    }

    private List<String> getWordsStartingWithChar(List<String> possibleWords, char startChar) {
        List<String> wordsStartingWithChar = possibleWords.stream().filter(word -> word.startsWith(String.valueOf(startChar))).collect(Collectors.toList());
        return wordsStartingWithChar.stream().filter(word ->
                (availableCharacters.size() - updateAvailableLetters(word).size()) == size)
                .collect(Collectors.toList());
    }

    private ArrayList<Character> adaptToArrayList(String letters) {
        ArrayList<Character> arrayList = new ArrayList<>();
        for (char letter : letters.toCharArray()) {
            arrayList.add(letter);
        }
        return arrayList;
    }
}
