package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author BarneyN
 * @since 27/03/2019
 */
public class WordService
{
	public List<String> getPossibleWords(Set charSet, List<String> wordList, int size)
	{
		List<String> nLengthWords = getNLengthWords(wordList, size);
		Character[] newArray = (Character[]) charSet.toArray(new Character[0]);
		return nLengthWords.stream().filter(word -> wordContainsValidCharacters(word, newArray)).collect(Collectors.toList());
	}

	public List<String> getPossibleWordsFromCharacterSet(ArrayList<Character> charSet, List<String> wordList, int size)
	{
		List<String> nLengthWords = getNLengthWords(wordList, size);
		Character[] newArray = charSet.toArray(new Character[0]);
		return nLengthWords.stream().filter(word -> wordContainsValidCharacters(word, newArray)).collect(Collectors.toList());
	}

	private List<String> getNLengthWords(List<String> wordList, int size)
	{
		return wordList.stream().filter(word -> word.length() == size).collect(Collectors.toList());
	}

	private boolean wordContainsValidCharacters(String word, Character[] charSet)
	{
		boolean isValidWord = false;
		for (char wordChar : word.toCharArray())
		{
			if (existsInChar(wordChar, charSet))
			{
				isValidWord = true;
			}
			else
			{
				isValidWord = false;
				break; // If any character is not in the character set break as word is invalid
			}
		}
		return isValidWord;
	}

	private boolean existsInChar(Character letter, Character[] charSet)
	{
		boolean result = false;
		for (char validChar: charSet)
		{
			if (letter == validChar)
			{
				result = true;
				break;
			}
		}
		return result;
	}
}
