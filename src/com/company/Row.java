package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BarneyN
 * @since 27/03/2019
 */
public class Row
{
	public Row(String word)
	{
		char[] chars = word.toCharArray();
		for (char currentChar: chars)
		{
			columns.add(new Column(currentChar));
		}

		this.word = word;
	}

	private String word;
	private List<Column> columns = new ArrayList<>();


	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public String getWord()
	{
		return word;
	}
}
