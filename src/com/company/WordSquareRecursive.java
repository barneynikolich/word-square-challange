package com.company;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author BarneyN
 * @since 29/03/2019
 */
public class WordSquareRecursive
{
	private int size;
	private ArrayList<Character> originalAvailableChars;
	private ArrayList<Character> availableCharacters;
	private ArrayList<String> wordList;
	private Set charSet = new HashSet();
	private WordService wordService = new WordService();
	private int attemptCounter;

	ArrayList<Row> rows = new ArrayList<>();

	ArrayList<Row> potentialRows = new ArrayList<>();

	WordSquareRecursive(int size, String letters, ArrayList wordList)
	{
		this.size = size;
		this.availableCharacters = adaptToArrayList(letters);
		this.originalAvailableChars = adaptToArrayList(letters);
		this.wordList = wordList;
		for (char letter : this.availableCharacters)
		{
			this.charSet.add(letter);
		}
	}

	public void solve()
	{
		List<String> availableWords = wordService.getPossibleWords(charSet, wordList, size);

		attemptCounter = 0;

		for (String word : availableWords)
		{
			this.availableCharacters = originalAvailableChars; // reset available characters as attempting next word
			potentialRows = new ArrayList<>(); // reset potential rows

			addPotentialRow(word);

			if (solveRows(2, potentialRows.get(0), availableWords))
			{
				break;
			}
			attemptCounter++;
		}

		System.out.println("Words attempted before solved: [" + attemptCounter + "]\n");
		this.rows.forEach(row -> System.out.println(row.getWord()));
	}

	public boolean solveRows(int charIndex, Row initialRow, List<String> availableWords)
	{
		List<String> specificWords = getPossibleWords(availableWords, initialRow.getColumns().get(charIndex - 1).getCharacter());

		if (specificWords.size() == 0)
		{
			// No words match available character set. Attempt failed try next word
			return false;
		}
		else
		{
			for (int currentWord = 0; currentWord < specificWords.size(); currentWord++)
			{
				resetAndUpdateAvailableCharacters(potentialRows.toArray(new Row[potentialRows.size()]));

				addPotentialRow(specificWords.get(currentWord));

				if (compareAllCells(charIndex))
				{

					if (charIndex == size)
					{
						this.rows.addAll(potentialRows);
						return true;
					}
					else
					{
						if (!solveRows(charIndex + 1, initialRow, availableWords))
						{
							potentialRows.remove(potentialRows.size() - 1); // Remove the last row added as the attempt failed
						}
						else
						{
							return true;
						}
					}
				}
				else
				{
					potentialRows.remove(potentialRows.size() - 1); // The characters at counterpart index don't match up. Attempt failed
				}
			}
		}
		return false;
	}

	private void updateAvailableCharacters(String word)
	{
		this.availableCharacters = updateAvailableLetters(word);
	}

	private List<String> getPossibleWords(List<String> availableWords, char startChar)
	{
		List<String> possibleWords = wordService.getPossibleWordsFromCharacterSet(availableCharacters, availableWords, size);
		return getWordsStartingWithChar(possibleWords, startChar);
	}

	private void resetAndUpdateAvailableCharacters(Row... rows)
	{
		this.availableCharacters = originalAvailableChars;
		for (Row row : rows)
		{
			this.availableCharacters = updateAvailableLetters(row.getWord());
		}
	}

	private void addPotentialRow(String word)
	{
		Row row = new Row(word);
		potentialRows.add(row);
		updateAvailableCharacters(row.getWord());
	}

	private ArrayList<Character> updateAvailableLetters(String usedLetters)
	{
		ArrayList<Character> availableChars = (ArrayList) availableCharacters.clone();

		for (Character usedLetter : usedLetters.toCharArray())
		{
			for (int i = 0; i < availableChars.size(); i++)
			{
				char available = availableChars.get(i);
				if (available == usedLetter)
				{
					availableChars.remove(i);
					break;
				}
			}
		}
		return availableChars;
	}

	private List<String> getWordsStartingWithChar(List<String> possibleWords, char startChar)
	{
		List<String> wordsStartingWithChar = possibleWords.stream().filter(word -> word.startsWith(String.valueOf(startChar))).collect(Collectors.toList());
		return wordsStartingWithChar.stream().filter(word ->
				(availableCharacters.size() - updateAvailableLetters(word).size()) == size)
				.collect(Collectors.toList());
	}

	public boolean compareAllCells(int squareSize)
	{
		for (int r = 0; r < squareSize; r++)
		{
			for (int c = 0; c < squareSize; c++)
			{
				if (!compareCells(r, c))
					return false;
			}
		}

		return true;
	}

	public boolean compareCells(int rowNumber, int columnNumber)
	{
		if (rowNumber == columnNumber)
			return true; // it's in the special diagonal - doesn't need to match with anything

		char thisChar = potentialRows.get(rowNumber).getColumns().get(columnNumber).getCharacter();
		char otherChar = potentialRows.get(columnNumber).getColumns().get(rowNumber).getCharacter();
		return thisChar == otherChar;
	}

	private ArrayList<Character> adaptToArrayList(String letters)
	{
		ArrayList<Character> arrayList = new ArrayList<>();
		for (char letter : letters.toCharArray())
		{
			arrayList.add(letter);
		}
		return arrayList;
	}
}
