package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * @author BarneyN
 * @since 27/03/2019
 */
public class WordListReader
{

	public ArrayList getWordsFromFile()
	{
		ArrayList wordList = new ArrayList();

		try (BufferedReader br = new BufferedReader(new FileReader("word-list.txt"))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				wordList.add(sCurrentLine);
			}

		} catch (IOException e) {
			System.out.println("Unable to find word-list on in resources. Attempting to get text file from URL (http://norvig.com/ngrams/enable1.txt)");
			return this.getFileFromUrl();
		}
		return wordList;
	}

	public ArrayList getFileFromUrl()
	{
		ArrayList wordList = new ArrayList();
		try {

			URL url = new URL("http://norvig.com/ngrams/enable1.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			while ((line = in.readLine()) != null) {
				wordList.add(line);
			}
			in.close();
		}
		catch (MalformedURLException e) {
			System.out.println("Malformed URL: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("I/O Error: " + e.getMessage());
		}

		return wordList;
	}

	public void writeToFile(ArrayList<String> wordList)
	{
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream("word-list.txt");
			for (String word: wordList)
			{
				byte[] strByte = word.getBytes();
				outputStream.write(strByte);
				String lineSeparator = System.getProperty("line.separator");
				outputStream.write(lineSeparator.getBytes());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
