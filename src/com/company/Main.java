package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String OPTION_1 = "aaccdeeeemmnnnoo";
    private static final String OPTION_2 = "aaaeeeefhhmoonssrrrrttttw";
    private static final String OPTION_3 = "aabbeeeeeeeehmosrrrruttvv";
    private static final String OPTION_4 = "aaaaaaaaabbeeeeeeedddddggmmlloooonnssssrrrruvvyyy";

    private static final List<String> CHOICES = Arrays.asList("a", "b", "c", "d");

    public static void main(String[] args) {

        WordListReader wordListReader = new WordListReader();
        ArrayList<String> wordList = wordListReader.getWordsFromFile();

        Scanner scanner = new Scanner(System.in);

        writeOptions();
        String choice = "";
        choice = scanner.next();

        while (!CHOICES.contains(choice))
        {
            writeOptions();
            choice = scanner.next();
        }

        WordSquareRecursive wordSquareRecursive;

        switch (choice)
        {
            case "a":
                wordSquareRecursive = new WordSquareRecursive(4, OPTION_1, wordList);
                wordSquareRecursive.solve();
                break;
            case "b":
                wordSquareRecursive = new WordSquareRecursive(5, OPTION_2, wordList);
                wordSquareRecursive.solve();
                break;
            case "c": wordSquareRecursive = new WordSquareRecursive(5, OPTION_3, wordList);
                wordSquareRecursive.solve();
                break;
            case "d":
                wordSquareRecursive = new WordSquareRecursive(7, OPTION_4, wordList);
                wordSquareRecursive.solve();
        }
    }

    private static void writeOptions()
    {
        System.out.println("Enter the challenge you would like to solve... (a, b, c or d)");
        System.out.println("a: 4 " + OPTION_1);
        System.out.println("b: 5 " + OPTION_2);
        System.out.println("c: 5 " + OPTION_3);
        System.out.println("d: 7 " + OPTION_4);
    }
}
